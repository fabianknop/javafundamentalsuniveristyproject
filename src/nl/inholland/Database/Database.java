package nl.inholland.Database;

import nl.inholland.Model.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Database {
    private List<Student> students;
    private List<Teacher> teachers;
    private List<Manager> managers;

    public void create(){
        this.students = new ArrayList<>() {{
            add(new Student(
                    1,
                    "emma",
                    "emma12",
                    "Emma",
                    "Smith",
                    parseStringToDate("12/04/1997"),
                    23,
                    "IT-02-A",
                    new Subject(54, 50, 66, 54)
            ));
            add(new Student(
                    2,
                    "jack",
                    "jack8",
                    "Jack",
                    "Brown",
                    parseStringToDate("08/07/1993"),
                    27,
                    "IT-02-A",
                    new Subject(72, 68, 43, 95)
            ));
            add(new Student(
                    3,
                    "michael",
                    "michael11",
                    "Michael",
                    "Garcia",
                    parseStringToDate("11/01/1999"),
                    21,
                    "IT-02-A",
                    new Subject(45, 71, 55, 84)
            ));
            add(new Student(
                    4,
                    "lisa",
                    "lisa4",
                    "Lisa",
                    "Jones",
                    parseStringToDate("04/29/2000"),
                    20,
                    "IT-02-A",
                    new Subject(98, 64, 81, 72)
            ));
            add(new Student(
                    5,
                    "john",
                    "john10",
                    "John",
                    "Miller",
                    parseStringToDate("10/27/2001"),
                    19,
                    "IT-02-A",
                    new Subject(100, 94, 99, 93)
            ));
            add(new Student(
                    6,
                    "linda",
                    "linda1",
                    "Linda",
                    "Martinez",
                    parseStringToDate("01/17/2002"),
                    18,
                    "IT-02-A",
                    new Subject(55, 79, 81, 55)
            ));
            add(new Student(
                    7,
                    "richard",
                    "richard9",
                    "Richard",
                    "Davis",
                    parseStringToDate("09/22/1997"),
                    23,
                    "IT-02-A",
                    new Subject(51, 64, 39, 59)
            ));
            add(new Student(
                    8,
                    "mark",
                    "mark12",
                    "Mark",
                    "Lopez",
                    parseStringToDate("12/09/1996"),
                    24,
                    "IT-02-A",
                    new Subject(78, 98, 89, 99)
            ));
            add(new Student(
                    9,
                    "debora",
                    "debora2",
                    "Debora",
                    "Hernandez",
                    parseStringToDate("02/25/1995"),
                    25,
                    "IT-02-A",
                    new Subject(59, 55, 67, 99)
            ));
            add(new Student(
                    10,
                    "rick",
                    "rick3",
                    "Rick",
                    "Moore",
                    parseStringToDate("03/16/2000"),
                    20,
                    "IT-02-A",
                    new Subject(96, 87, 55, 82)
            ));
            add(new Student(
                    11,
                    "paul",
                    "michael11",
                    "Paul",
                    "Smith",
                    parseStringToDate("12/23/1998"),
                    22,
                    "IT-02-B",
                    new Subject()
            ));
        }};

        this.teachers = new ArrayList<>() {{
            add(new Teacher(
                    1,
                    "david",
                    "david6",
                    "David",
                    "Taylor",
                    parseStringToDate("06/15/1965"),
                    55,
                    5200.0
            ));
        }};

        this.managers = new ArrayList<>() {{
            add(new Manager(
                    1,
                    "ben",
                    "ben12",
                    "Ben",
                    "Johnson",
                    parseStringToDate("08/04/1998"),
                    22
            ));
        }};
    }

    public List<User> getAll(){
        List<User> users = new ArrayList<>();
        users.addAll(this.students);
        users.addAll(this.teachers);
        users.addAll(this.managers);
        return users;
    }

    public List<Student> getAllStudents() {
        return this.students;
    }

    public List<Teacher> getAllTeachers() {
        return this.teachers;
    }

    public List<Manager> getAllManagers() {
        return this.managers;
    }

    public void addStudent(int id, String username, String password, String firstName, String lastName, Date birthDate, String group){
        this.students.add(
                new Student(id, username, password, firstName, lastName, birthDate, 20, group, new Subject())
        );
    }

    public void updateGradesById(int id, int java, int cSharp, int python, int php){
        for(Student student : this.students){
            if(student.getId() == id){
                student.setSubjects(java, cSharp, python, php);
                this.students.set(id-1, student);
            }
        }
    }

    public Date parseStringToDate(String date){
        Date date1 = null;
        try{
            date1 = new SimpleDateFormat("dd/MM/yyyy").parse(date);
        } catch(ParseException e) {
            System.err.println("Please enter a valid date according to the format");
        }
        return date1;
    }
}
