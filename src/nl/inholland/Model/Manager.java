package nl.inholland.Model;

import nl.inholland.Enum.AccessLevel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Manager extends User
{
    public Manager(
            int id,
            String username,
            String password,
            String firstName,
            String lastName,
            Date birthDate,
            int age
    ) {
        super(id, username, password, firstName, lastName, birthDate, age, AccessLevel.ADMIN);
    }

    @Override
    String userToString() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String strDate = dateFormat.format(this.birthDate);
        return id + "\t" + firstName + "\t" + lastName + "\t" + strDate + "\t" + age;
    }
}
