package nl.inholland.Model;

import nl.inholland.Enum.AccessLevel;

import java.util.Date;

public abstract class User {
    protected int id;
    protected String username;
    protected String password;
    protected String firstName;
    protected String lastName;
    protected Date birthDate;
    protected int age;
    protected AccessLevel accessLevel;

    public User (
            int id,
            String username,
            String password,
            String firstName,
            String lastName,
            Date birthDate,
            int age,
            AccessLevel accessLevel
    ) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.age = age;
        this.accessLevel = accessLevel;
    }

    public int getId(){
        return this.id;
    }

    public String getUsername(){
        return this.username;
    }

    public String getPassword(){
        return this.password;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName(){
        return this.lastName;
    }

    public int getAge(){
        return this.age;
    }

    public AccessLevel getAccessLevel(){
        return this.accessLevel;
    }

    abstract String userToString();
}
