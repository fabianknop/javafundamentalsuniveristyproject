package nl.inholland.Model;

import nl.inholland.Enum.AccessLevel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Student extends User {

    private String group;
    private Subject subjects;

    public Student(
            int id,
            String username,
            String password,
            String firstName,
            String lastName,
            Date birthDate,
            int age,
            String group,
            Subject subjects
    ) {
        super(id, username, password, firstName, lastName, birthDate, age, AccessLevel.BASIC);
        this.group = group;
        this.subjects = subjects;
    }

    public Subject getSubjects(){
        return this.subjects;
    }

    public void setSubjects(int java, int cSharp, int python, int php){
        this.subjects = new Subject(java, cSharp, python, php);
    }

    @Override
    public String userToString() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String strDate = dateFormat.format(this.birthDate);
        return id + "\t" + firstName + "\t" + lastName + "\t" + strDate + "\t" + age + "\t" + group;
    }

    public String reportToString(){
        return this.userToString() + subjects.toString();
    }
}
