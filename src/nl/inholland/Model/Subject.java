package nl.inholland.Model;

import java.util.ArrayList;
import java.util.List;

public class Subject {

    private int java;
    private int cSharp;
    private int python;
    private int php;

    public Subject(){
        this.java = 0;
        this.cSharp = 0;
        this.python = 0;
        this.php = 0;
    }

    public Subject(int java, int cSharp, int python, int php){
        this.java = java;
        this.cSharp = cSharp;
        this.python = python;
        this.php = php;
    }

    public int getJava(){
        return this.java;
    }

    public int getcSharp(){
        return this.cSharp;
    }

    public int getPython(){
        return this.python;
    }

    public int getPhp(){
        return this.php;
    }

    public List<Integer> getAllGrades(){
        return new ArrayList<>() {{
            add(java);
            add(cSharp);
            add(python);
            add(php);
        }};
    }

    @Override
    public String toString() {
        return "\t" + this.java + "\t" + cSharp + "\t" + python + "\t" + php;
    }
}
