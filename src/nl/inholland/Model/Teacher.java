package nl.inholland.Model;

import nl.inholland.Enum.AccessLevel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Teacher extends User{

    private double salary;

    public Teacher(
            int id,
            String username,
            String password,
            String firstName,
            String lastName,
            Date birthDate,
            int age,
            double salary
    ) {
        super(id, username, password, firstName, lastName, birthDate, age, AccessLevel.EDITOR);
        this.salary = salary;
    }

    @Override
    public String userToString() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String strDate = dateFormat.format(this.birthDate);
        return this.id + "\t" + this.firstName + "\t" + this.lastName + "\t" + strDate + "\t" + age + "\t" + salary;
    }
}
