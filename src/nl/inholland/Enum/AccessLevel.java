package nl.inholland.Enum;

public enum AccessLevel {
    BASIC,
    EDITOR,
    ADMIN
}
