package nl.inholland.Handler;

import nl.inholland.Database.Database;
import nl.inholland.Enum.Option;
import nl.inholland.Model.Teacher;
import nl.inholland.Model.User;

import java.util.List;

public class DisplayTeachersHandler extends BaseHandler{
    @Override
    public void handle(Option option, User user, Database database){
        System.out.printf(" LIST OF TEACHERS %n%n");
        System.out.printf("%s%14s%13s%14s%8s%11s%n", "Id", "FirstName", "LastName", "BirthDate", "Age", "Salary");
        System.out.printf("%s%14s%13s%14s%8s%11s%n", "**", "*********", "********", "*********", "***", "******");
        List<Teacher> teachers = database.getAllTeachers();
        for(Teacher teacher : teachers){
            System.out.println(teacher.userToString());
        }
        System.out.println();
    }
}
