package nl.inholland.Handler;

import nl.inholland.ConsoleApp;
import nl.inholland.Database.Database;
import nl.inholland.Enum.Option;
import nl.inholland.Model.Student;
import nl.inholland.Model.User;

import java.util.Scanner;

public class UpdateReportHandler extends BaseHandler{
    @Override
    public void handle(Option chosenOption, User user, Database db) {
        Scanner input = new Scanner(System.in);
        Option choice = null;
        while (choice == null) {
            System.out.printf("%nA. Add (update) report | R. Display reports | B. Back to main | X. Exit |%n%n Select a menu: ");
            String inputChoice = input.nextLine().toUpperCase();
            for (Option option : Option.values()) {
                if (option.toString().equals(inputChoice)) {
                    choice = option;
                    break;
                }
            }
        }
        switch(choice){
            case A:
                this.updateReport(user, input, db);
                this.handle(chosenOption, user, db);
                break;
            case R:
                System.out.printf("display report ..................%n%n");
                ConsoleApp consoleApp = new ConsoleApp();
                consoleApp.handleAction(choice, user, db);
                break;
            case B:
                break;
            case X: System.exit(0);
        }
    }

    private void updateReport(User user, Scanner input, Database db){
        Student student = (Student) user;
        System.out.printf("%nCurrent grade java is: " + student.getSubjects().getJava() + " Enter (new) grade: ");
        int newJavaGrade = input.nextInt();
        System.out.printf("%nCurrent grade CShrap is: " + student.getSubjects().getcSharp() + " Enter (new) grade: ");
        int newCSharpGrade = input.nextInt();
        System.out.printf("%nCurrent grade Python is: " + student.getSubjects().getPython() + " Enter (new) grade: ");
        int newPythonGrade = input.nextInt();
        System.out.printf("%nCurrent grade PHP is: " + student.getSubjects().getPhp() + " Enter (new) grade: ");
        int newPhpGrade = input.nextInt();
        db.updateGradesById(student.getId(), newJavaGrade, newCSharpGrade, newPythonGrade, newPhpGrade);
    }
}
