package nl.inholland.Handler;

import nl.inholland.Database.Database;
import nl.inholland.Enum.Option;
import nl.inholland.Model.User;

public abstract class BaseHandler {
    protected String key;

    public void handle(Option option, User user, Database db){}
}
