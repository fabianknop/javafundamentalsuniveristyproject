package nl.inholland.Handler;

import nl.inholland.Database.Database;
import nl.inholland.Enum.Option;
import nl.inholland.Model.Student;
import nl.inholland.Model.User;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class AddStudentHandler extends BaseHandler{
    @Override
    public void handle(Option option, User user, Database database){
        Scanner input = new Scanner(System.in);

        System.out.printf(" ADD STUDENT %n%n");
        System.out.print("Choose a username: ");
        String username = input.nextLine();
        System.out.printf("%nChoose a password: ");
        String password = input.nextLine();
        System.out.printf("%nEnter first name: ");
        String firstName = input.nextLine();
        System.out.printf("%nEnter last name: ");
        String lastName = input.nextLine();
        Date birthDate;
        while(true){
            try{
                System.out.printf("%nPlease enter date of birth in MM/DD/YYYY: ");
                String dateOfBirth = input.nextLine();
                birthDate = new SimpleDateFormat("dd/MM/yyyy").parse(dateOfBirth);
                break;
            } catch (ParseException e){
                System.err.println("Please enter a valid date according to the format.");
                input.next();
            }
        }
        System.out.printf("%nEnter group: ");
        String group = input.nextLine();

        List<Student> students = database.getAllStudents();
        Student student = students.get(students.size() - 1);
        database.addStudent(student.getId() + 1, username, password, firstName, lastName, birthDate, group);
        System.out.printf("%nThe data was successfully added!%n%n");
    }
}
