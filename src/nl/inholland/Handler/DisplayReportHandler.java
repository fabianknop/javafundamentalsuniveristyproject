package nl.inholland.Handler;

import nl.inholland.Database.Database;
import nl.inholland.Enum.Option;
import nl.inholland.Model.Student;
import nl.inholland.Model.User;

import java.util.List;

public class DisplayReportHandler extends BaseHandler{
    @Override
    public void handle(Option option, User user, Database database) {
        System.out.printf(" STUDENT RESULTS %n%n");
        System.out.printf("%s%14s%13s%14s%8s%10s%9s%11s%11s%8s%n", "Id", "FirstName", "LastName", "BirthDate", "Age", "Group", "Java", "CSharp", "Python", "PHP");
        System.out.printf("%s%14s%13s%14s%8s%10s%9s%11s%11s%8s%n", "**", "*********", "********", "*********", "***", "*****", "****", "******", "******", "***");
        List<Student> students = database.getAllStudents();
        for(Student student : students){
            System.out.println(student.reportToString());
        }
        System.out.println();
    }
}
