package nl.inholland.Handler;

import nl.inholland.Database.Database;
import nl.inholland.Enum.Option;
import nl.inholland.Model.Student;
import nl.inholland.Model.User;

import java.io.*;

public class SaveReportsHandler extends BaseHandler {
    @Override
    public void handle(Option option, User user, Database db) {
        PrintStream stdOut = System.out;
        DisplayReportDetailsHandler displayReportDetailsHandler = new DisplayReportDetailsHandler();
        for (Student student : db.getAllStudents()) {
            try{
                PrintStream out = new PrintStream(new FileOutputStream(new File("reports/" + student.getId() + " " + student.getFirstName() + " " + student.getLastName())));
                System.setOut(out);
                displayReportDetailsHandler.printReport(student);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        System.setOut(stdOut);
    }
}
