package nl.inholland.Handler;

import nl.inholland.Database.Database;
import nl.inholland.Enum.Option;
import nl.inholland.Model.Student;
import nl.inholland.Model.User;

import java.util.List;

public class DisplayReportDetailsHandler extends BaseHandler {
    @Override
    public void handle(Option option, User user, Database db) {
        Student student = (Student) user;
        this.printReport(student);
        UpdateReportHandler updateReportHandler = new UpdateReportHandler();
        updateReportHandler.handle(option, student, db);
    }

    public void printReport(Student student){
        System.out.printf("%n%nReport of student " + student.getFirstName() + " " + student.getLastName() + "%n%n");
        System.out.println("Student Id: " + student.getId());
        System.out.println("First Name: " + student.getFirstName());
        System.out.println("Last Name: " + student.getLastName());
        System.out.println("Age: " + student.getAge());
        System.out.printf("%n\t\tCOURSES%n%n");
        System.out.println("Java: " + student.getSubjects().getJava());
        System.out.println("CSharp: " + student.getSubjects().getcSharp());
        System.out.println("Python: " + student.getSubjects().getPython());
        System.out.println("PHP: " + student.getSubjects().getPhp());
        System.out.printf("%n\t\tRESULTS%n%n");

        List<Integer> grades = student.getSubjects().getAllGrades();
        int retakes = 0;
        for(int grade : grades){
            if(grade < 55){
                retakes++;
            }
        }
        String result = "Not Passed";
        if(retakes == 0){
            result = "Passed";
        }
        System.out.println("Result: " + result);
        System.out.println("Retakes: " + retakes);
    }
}
