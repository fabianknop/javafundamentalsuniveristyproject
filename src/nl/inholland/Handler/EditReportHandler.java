package nl.inholland.Handler;

import nl.inholland.Database.Database;
import nl.inholland.Enum.Option;
import nl.inholland.Model.Student;
import nl.inholland.Model.User;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class EditReportHandler extends BaseHandler {
    @Override
    public void handle(Option option, User user, Database database) {
        Scanner input = new Scanner(System.in);
        int studentId;
        List<Student> students = database.getAllStudents();
        Student student = null;
        while (true) {
            try {
                System.out.print(" Enter student id (report details) | Or 0 to go back to main menu: ");
                studentId = input.nextInt();
                if (studentId == 0) {
                    return;
                }
                for (Student inputStudent : students) {
                    if (inputStudent.getId() == studentId) {
                        student = inputStudent;
                        break;
                    }
                }
                if (student != null) {
                    break;
                }
            } catch (InputMismatchException e) {
                System.err.println("Please enter a number!");
                input.nextLine();
            }
        }
        DisplayReportDetailsHandler displayReportDetailsHandler = new DisplayReportDetailsHandler();
        displayReportDetailsHandler.handle(option, student, database);
    }
}
