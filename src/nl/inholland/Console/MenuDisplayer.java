package nl.inholland.Console;

import nl.inholland.Enum.AccessLevel;

public class MenuDisplayer {
    public void display(AccessLevel accessLevel){
        switch(accessLevel){
            case BASIC:
                this.displayBasicMenu();
                this.displayExit();
                break;
            case EDITOR:
                this.displayEditorMenu();
                this.displayExit();
                break;
            case ADMIN:
                this.displayAdminMenu();
                this.displayExit();
                break;
        }
    }

    private void displayBasicMenu(){
        System.out.print("S. Display Students | T. Display Teachers | ");
    }

    private void displayEditorMenu(){
        this.displayBasicMenu();
        System.out.print("A. Add Students | R. Display Reports | ");
    }

    private void displayAdminMenu(){
        this.displayEditorMenu();
        System.out.print("V. Save Reports | ");
    }

    private void displayExit(){
        System.out.printf("X. Exit | %n%nPlease, enter a choice: ");
    }
}
