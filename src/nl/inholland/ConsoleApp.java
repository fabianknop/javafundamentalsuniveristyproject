package nl.inholland;

import nl.inholland.Database.Database;
import nl.inholland.Enum.Option;
import nl.inholland.Handler.*;
import nl.inholland.Model.User;
import nl.inholland.Console.MenuDisplayer;

import java.util.List;
import java.util.Scanner;

public class ConsoleApp {
    public void run() {
        Database db = new Database();
        db.create();
        List<User> users = db.getAll();
        User user = this.login(users);
        Option option = null;
        while (option != Option.X) {
            option = this.chooseAction(user);
            this.handleAction(option, user, db);
        }
    }

    private User login(List<User> users) {
        Scanner input = new Scanner(System.in);
        User loggedInUser = null;
        while (loggedInUser == null) {
            System.out.print("Enter username: ");
            String username = input.nextLine();
            System.out.print("Enter password: ");
            String password = input.nextLine();
            for (User user : users) {
                if (user.getUsername().equals(username) && user.getPassword().equals(password)) {
                    loggedInUser = user;
                }
            }
        }
        return loggedInUser;
    }

    public Option chooseAction(User user) {
        Scanner input = new Scanner(System.in);
        MenuDisplayer menuDisplayer = new MenuDisplayer();
        Option action = null;
        while (action == null) {
            menuDisplayer.display(user.getAccessLevel());
            String chosenAction = input.nextLine().toUpperCase();
            for (Option option : Option.values()) {
                if (option.toString().equals(chosenAction)) {
                    action = option;
                    break;
                }
            }
        }
        return action;
    }

    public void handleAction(Option option, User user, Database db) {
        switch (option) {
            case S:
                DisplayStudentsHandler displayStudentsHandler = new DisplayStudentsHandler();
                displayStudentsHandler.handle(option, user, db);
                break;
            case T:
                DisplayTeachersHandler displayTeachersHandler = new DisplayTeachersHandler();
                displayTeachersHandler.handle(option, user, db);
                break;
            case A:
                AddStudentHandler addStudentHandler = new AddStudentHandler();
                addStudentHandler.handle(option, user, db);
                break;
            case R:
                DisplayReportHandler displayReportHandler = new DisplayReportHandler();
                displayReportHandler.handle(option, user, db);
                EditReportHandler editReportHandler = new EditReportHandler();
                editReportHandler.handle(option, user, db);
                break;
            case V:
                SaveReportsHandler saveReportsHandler = new SaveReportsHandler();
                saveReportsHandler.handle(option, user, db);
                break;
            case X:
                break;
        }
    }
}
